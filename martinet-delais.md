#   Etude des délais synaptiques dans les réseaux de neurones impulsionnels pour la vision

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

Le projet proposé se situe dans le domaine de la vision par ordinateur. Il porte sur sur la reconnaissance de motifs temporels dans les vidéos à l’aide de réseaux de neurones impulsionnels (Spiking Neural Networks, SNN). Il vise l’utilisation d’un capteur vidéo non-conventionnel qui encode les variations de contraste temporel de manière binaire au niveau de chaque pixel. Le travail demandé vise à étudier l'utilisation de délais synaptiques (délais entre l'émission et la réception d'une impulsion) afin de permettre la reconnaissance de motifs selon leur aspect, leur vélocité, etc. Ce sujet entre dans le cadre plus large d’un projet dont l’objectif est de proposer une approche de vision bio-inspirés, du capteur au traitement de l’information visuelle.

### Mots-clés

vision artificielle, réseaux de neurones impulsionnels, délai synaptique, vidéo, apprentissage, bio-inspiration

### Encadrement

Équipe : FOX, CRIStAL

Encadrant(s) :

-   Jean Martinet
-   Veïs Oudjail

[Contacter les encadrants](mailto:jean.martinet@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : IRCICA, Parc Scientifique de la Haute Borne, 50 Avenue Halley, 59658 Villeneuve d’Ascq


##  Présentation détaillée

### Pré-requis

Le candidat est un étudiant de Master mention Informatique, idéalement IVI ou MOCAD, et montre un grand intérêt pour la recherche. Un connaissance du langage Python est nécessaire. Des aptitudes à la rédaction d'articles scientifiques sont appréciées.

### Contexte

Le projet proposé se situe dans le domaine de la vision par ordinateur. Il porte sur sur la reconnaissance de motifs temporels dans les vidéos à l’aide de réseaux de neurones impulsionnels (Spiking Neural Networks, SNN). Il s’agit d’un type particulier de modèle de neurones, proche du modèle biologique, dans lequel les neurones émettent des impulsions sortantes (spikes) de manière asynchrone, en fonction des stimulations entrantes, asynchrones elles aussi. L’apprentissage avec de tels réseaux est non supervisé, et repose sur une règle “Spike-Timing Dependent Plasticity”, qui met à jour les poids synaptiques au cours d'une simulation en fonction des relations de cause à effet constatées entre les impulsions entrantes et sortantes. Le but de cette règle est le renforcement des connexions entrantes qui sont la cause des impulsions sortantes.

### Problématique

Nous nous intéressons dans ce stage orienté Recherche à l’utilisation des délais synaptiques dans les SNN, pour le traitement des données visuelles issues de capteurs “non-conventionnels”, qui se distinguent des capteurs classiques fournissant des séquences de matrices représentant les pixels au format RBG des images successives de la scène. Parmi les capteurs que nous envisageons, les capteurs de type DVS (https://inilabs.com/products/dynamic-vision-sensors/) représentent l’information au format Address-Event Representation (AER). Ce format encode de manière binaire, indépendamment pour chaque pixel, une variation de luminosité (positive ou négative). Ainsi, chaque variation d’un pixel (x,y) à l’instant (t) se traduit par un événement (x,y,t) transmis de manière asynchrone. Par conséquent, une scène immobile ne génèrera aucun événement (autrement dit, on ne “voit” que les mouvements), ce qui élimine une grande partie de redondance dans l’information. Le caractère asynchrone des SNN se prête bien au traitement de l’information issue de ces capteurs : leurs sortie peut être traitée et convertie pour nourrir la couche d’entrée du réseau.

Les détecteurs de Reichardt (https://isle.hanover.edu/Ch08Motion/Ch08ReichardtDetectors.html) forment une hypothèse sur la manière dont le cerveau perçoit le mouvement. Supposons qu'un neurone reçoive un signal (impulsion électrique) de la part de deux photorécepteurs A et B connexes de l'oeil, sachant que le signal en provenance de A présente un délai. Si le mouvement de A vers B est temporisé de sorte que le neurone reçoive les signaux de A et B en même temps, alors l'addition des signaux permettra l'activation du neurone. Ces détecteurs sont généralisés par la notion d'assemblées de neurones polychrones, qui permettent la reconnaissance d'un motif temporel d'émission de spikes.

### Travail à effectuer

L’objectif est d’étudier l’utilité et la manière d’exploiter les délais synaptiques, les détecteurs de Reichardt et la polychronie pour la reconnaissance de motifs visuels dans les vidéos. Dans un premier temps, il s’agira dans ce stage de se familiariser avec les SNN et le capteur envisagé.

Ensuite, on proposera un cadre d’utilisation de délais fixes ou adaptatifs afin de reconnaître des motifs simples (déplacement d'un pixel, d'un motif donné), voir de motifs quelconques. Puis, il s'agira d'assembler des détecteurs élémentaires afin de pouvoir détecter des motifs de mouvements plus complexes.

On procèdera enfin à une validation expérimentale du cadre proposé à l'aide du simulateur Brian (http://briansimulator.org).

### Bibliographie

- [Reichardt, 1987] Werner Reichardt : Evaluation of optical motion information by movement detectors. Journal of Comparative Physiology A, 1987. DOI:10.1007/BF00603660.

- [Ponulak, 2011] Filip Ponulak, Andrzej Kasiński. Introduction to spiking neural networks: Information processing, learning and applications. Acta Neurobiol Exp (Wars). 2011;71(4):409-33.

- [Bichler, 2012] Olivier Bichler, Damien Querlioz, Simon J. Thorpe, Jean-Philippe Bourgoin, Christian Gamrat : Extraction of temporally correlated features from dynamic vision sensors with spike-timing-dependent plasticity. Neural Networks 32 (2012) 339–348.

- [Paugam-Moisy, 2012] Hélène Paugam-Moisy, Sander Bohte : Computing with Spiking Neuron Networks. Handbook of Natural Computing, pp 335-376.

- [Oudjail, 2019] Veïs Oudjail, Jean Martinet. Bio-inspired event-based motion analysis with spiking neural networks. International Conference on Computer Vision Theory and Applications (VISAPP'19), Feb 25-27 2019,  Prague, Czech Republic (https://hal.archives-ouvertes.fr/hal-01940917v1)
