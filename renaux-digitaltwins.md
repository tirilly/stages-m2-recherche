#   Information System Digital Twins 

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

L’objectif de ce stage est d’appliquer la science des données pour aider la société Axellience à accompagner ses clients dans leur transformation digital. Axellience est spécialisée dans la modélisation des processus d’entreprise et conceptrice de l’outil de modélisation dans le cloud GenMyModel. Dans ce domaine la société souhaite adapter à ses pratiques le concept de Jumeau Numérique (Digital Twin). L’équipe CARBON du laboratoire CRIStAL mène une partie de sa recherche sur l'étude de l'utilisation de la modélisation pour la conception logicielle. L’objectif de l’équipe est d’innover dans les domaines de la modélisation et de l’interaction de l’humain avec les nouveaux outils de modélisation. Elle se concentrera dans ce travail sur l’étude des nouveaux processus de production logicielle. 
Le cœur de ce projet est de récolter et de traiter, sur la base des techniques d’intelligence artificielle,  toutes les données issues d’un système physique. L’objectif est de créer et adapter le double numérique d’un tel système et de réaliser des simulations et prédictions pour améliorer la qualité dans la gestion des systèmes d’information. 
A la fin du stage, en fonction de votre projet professionnel, vous aurez la possibilité d’effectuer une thèse de type CIFRE (thèse en laboratoire et en entreprise).

### Mots-clés

Modèles logiciels, processus, intelligence artificielle, visualisation, data mining

### Encadrement

Équipe : CARBON (CRIStAL)

Encadrant(s) :

- Emmanuel Renaux

Société : Axellience

Responsable :

- Alexis Muller

[Contacter les encadrants](mailto:emmanuel.renaux@imt-lille-douai.fr?subject=Stage%20de%20recherche).

Localisation : Société Axellience, Euratechnologie


##  Présentation détaillée

### Pré-requis

- Modélisation : Eclipse Modeling Framework
- Framework : Spring, JPA, Vue GWT
- HTML / CSS
- Outils : Eclipse, Git, Maven, JIRA, Jenkins
- Autonome et travail en équipe
- Intérêt prononcé pour les nouvelles technologies et l'innovation

### Contexte

Axellience est une startup issue des laboratoires d'Inria, plusieurs fois primée : concours national de création d'entreprises innovantes "émergence" 2011 et "création développement" 2012, concours des entreprises innovantes "tremplin entreprise" du Sénat 2013. Axellience est éditeur de logiciels de la première plateforme de modélisation entièrement web : GenMyModel. Axellience innove en proposant enfin des outils de modélisation modernes, performants et agréables à utiliser. GenMyModel regroupe à ce jour plus de 450 000 utilisateurs enregistrés répartis dans plus de 150 pays. Axellience compte parmi ses clients de grands acteurs internationaux et notamment de grands acteurs nord-américains. Axellience compte aujourd’hui une dizaine de collaborateurs internes et externes. Son siège se situe à Lille dans le quartier d’Euratechnologies.
L'équipe CARBON du laboratoire CRIStAL mène une partie de sa recherche sur l'étude de l'utilisation de la modélisation pour la conception logicielle. Dans le cadre de son activité de recherche empirique, l'équipe mène des expérimentations pour analyser l'efficacité visuelle du diagramme et de son contenu et les interactions avec les outils de modélisation à destination des concepteurs. L’équipe collabore avec la société Axellience lorsque des questions de fonds se posent et peuvent aider à promouvoir l’intérêt de la modélisation dans le monde professionnel. 


### Problématique

Le jumeau numérique est une innovation du modèle de gestion du cycle de vie des produits (PLM - Product Lifecycle Management) de l’industrie qui vise à créer et à entretenir les produits industriels tout au long de leur cycle de vie. Les modèles, réalisés grâce à la société Axellience, participeront à la constitution de jumeaux numériques des systèmes d’information (ou systèmes physiques) de ses clients.
Dans un fonctionnement normal, le système physique et son jumeau numérique doivent être synchronisés tout au long de leur cycle de vie. La problématique est de maintenir cette synchronisation par l’observation et le traitement des nombreuses données récoltées depuis le système physique (au travers de divers capteurs ou sondes). L’objectif est de prévoir, en se basant sur le jumeau numérique, un problème potentiel avant qu’il ne se produise dans le système réel.
Les besoins de visualisation et de traitement de flux de données massives en provenance du système physique et de son environnement nous amènent à nous tourner vers les sciences des données et l’intelligence artificielle comme solution. Ceci afin d’anticiper un dysfonctionnement et de proposer les évolutions préventives des systèmes. 

### Travail à effectuer

Une première étape pourrait être de collecter et traiter les données discrètes et de les lier avec des éléments du modèle. Il s’agirait de collecter les données qui proviennent du système physique, via des capteurs/sondes, et de les intégrer dans le double numérique. Il est donc nécessaire d’étudier les standards d’échange et d’intégration existants, notamment dans les domaines de l’internet des objets (IOT), systèmes cyber-physiques ou industrie 4.0. Ensuite, il faudra définir et implémenter des moyens de visualisation (tableaux, graphiques, …) et sélectionner des techniques de machine learning afin d’exploiter ces données et créer de la valeur pour les utilisateurs (prédictions de fautes systèmes, évaluations de propriétés non-fonctionnelles telles que la résilience, ...).

### Bibliographie

[1] Tao et al. Digital twin-driven product design, manufacturing and service with big data, International Journal of Advanced Manufacturing Technology, March 2017
[2] Eric J. Tuegel, Anthony R. Ingraffea, Thomas G. Eason, and S. Michael Spottswood, Reengineering Aircraft Structural Life Prediction Using a Digital Twin, International Journal of Aerospace Engineering Volume 2011
[3] E. H. Glaessgen, D.S. Stargel, The Digital Twin Paradigm for Future NASA and U.S. Air Force Vehicles, 53rd Structures, Structural Dynamics, and Materials Conference: Special Session on the Digital Twin

